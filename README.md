# Jóvenes Construyendo el Futuro

Portal informativo para reportar al público general la actividad de SC en el programa JCF.

El objetivo es dar a conocer los Centros de la SC, y mostrar algunos de los resultados ahí generados.

La principal del sitio es la redirección al sitio de JCF, así como el envío de mensajes de contacto en el sitio o vía la activación del link al e-mail oficial.





# Interacción

## Navegación

El sitio web está conformado como una única columna de scroll.

El usuario puede saltar a las distintas secciones utilizando el menú.

## Interacción Mapa:

- Usuario activa una Entidad Federativa
- Se muestra listado de Centros de Entidad Federativa seleccionada
- Usuario elige Centro
    - Se muestra Detalle de Centro seleccionado
        - Botón "Volver"
        - Usuario abre un Trabajo
            - Botón "Volver"
            - Se muestra Detalle de Trabajo
        - Usuario abre Capacitación 
            - Botón "Volver"
            - Se muestra Detalle de Capacitación

## Contacto

- Usuario rellena y envía formulario
- Usuario abre enlaces externos

# Entidades


- [Centro](Entidades/Centro.md)
- [Trabajo](Entidades/Trabajo.md)
- [Capacitación](Entidades/Capacitación.md)


 

### Inicio:

- Cabecera
    - Logotipo JCF
    - Logotipo SC
    - Menú
        - Presentación
        - Centros
        - Contacto

- Contenido:

    - Manifiesto
        - Texto 2-3 párrafos
    - Video: Presentación
        - Video embebido
    - Mapa de Centros
        - Mapa interactivo de Centros en entidades federativas
    
- Contacto
    - Texto: 1 párrafo
    - Formulario
    - Enlace a Sitio Oficial Programa J.C.F.


- Pie de Página

    - Derechos Reservados
    - Enlaces
        - JCF
        - SC
        - CCD
    - Aviso de Privacidad



### Componentes:

- Cabecera
    - Logotipos
    - Menú
- Portada
    - Imagen de fondo ?
    - Título
    - Subtítulo

- Contenedor Manifiesto
- Contenedor Video Presentación
- Mapa interactivo
    - Listado Entidades Federativas
        - Listado Localidades
            - Listado Centros
- Navegación Local: Botón "Volver"
- Vista Previa Centro
- Detalle Centro
- Vista Previa Trabajo
- Detalle Trabajo
- Vista Detalle Capacitación
- Texto Contacto
- Formulario de Contacto

